package com.bankaya.proxy.test.business;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bankaya.proxy.test.domain.Tracking;
import com.bankaya.proxy.test.repository.TrackingRepository;
import com.bankaya.proxy.test.service.ITrackingService;
import com.bankaya.proxy.test.utils.TrackingUtils;

/**
 * 
 * @author Enrique
 *
 */
@Service
public class TrackingBusiness implements ITrackingService {

	@Autowired
	private TrackingRepository trackingRepository;

	@Override
	public Tracking save(HttpServletRequest request, String methodName) {
		return trackingRepository.save(Tracking.builder().ip(TrackingUtils.getIpFromHttpRequest(request))
				.creationDate(new Date()).method(methodName).build());
	}

}
