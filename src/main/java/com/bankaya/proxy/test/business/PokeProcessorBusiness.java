package com.bankaya.proxy.test.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bankaya.proxy.test.client.IPokemonClientRest;
import com.bankaya.proxy.test.model.Pokemon;
import com.bankaya.proxy.test.schema.GetAbilities;
import com.bankaya.proxy.test.schema.GetBaseExperience;
import com.bankaya.proxy.test.schema.GetHeldItems;
import com.bankaya.proxy.test.schema.GetId;
import com.bankaya.proxy.test.schema.GetLocationAreaEncounters;
import com.bankaya.proxy.test.schema.GetName;
import com.bankaya.proxy.test.service.IPokeProcessorService;

/**
 * 
 * @author Enrique
 *
 */
@Service
public class PokeProcessorBusiness implements IPokeProcessorService{

	@Autowired
	private IPokemonClientRest pokemonClientRest;

	@Override
	public GetAbilities getAbilities(String name) {
		ResponseEntity<Pokemon>  response = pokemonClientRest.getPokemon(name.trim().toLowerCase());
		
		if(response.getStatusCode() == HttpStatus.OK) { 
			 GetAbilities abilities = new GetAbilities();
			 abilities.setAbilities(response.getBody().getAbilities());
			 
			 return abilities;
		} 
		
		return null;
	}

	@Override
	public GetBaseExperience getBaseExperience(String name) {
		ResponseEntity<Pokemon>  response = pokemonClientRest.getPokemon(name.trim().toLowerCase());
		
		if(response.getStatusCode() == HttpStatus.OK) { 
			GetBaseExperience experience = new GetBaseExperience();
			experience.setBaseExperience(response.getBody().getBaseExperience());
			 
			 return experience;
		} 
		
		return null;
	}

	@Override
	public GetHeldItems getHeldItems(String name) {
		ResponseEntity<Pokemon>  response = pokemonClientRest.getPokemon(name.trim().toLowerCase());
		
		if(response.getStatusCode() == HttpStatus.OK) { 
			GetHeldItems items = new GetHeldItems();
			items.setHeldItems(response.getBody().getHeldItems());
			 
			 return items;
		} 
		
		return null;
	}

	@Override
	public GetId getId(String name) {
		ResponseEntity<Pokemon>  response = pokemonClientRest.getPokemon(name.trim().toLowerCase());
		
		if(response.getStatusCode() == HttpStatus.OK) { 
			GetId id = new GetId();
			id.setId(response.getBody().getId());
			 
			 return id;
		} 
		
		return null;
	}

	@Override
	public GetLocationAreaEncounters getLocationAreaEncounters(String name) {
		ResponseEntity<Pokemon>  response = pokemonClientRest.getPokemon(name.trim().toLowerCase());
		
		if(response.getStatusCode() == HttpStatus.OK) { 
			GetLocationAreaEncounters encounters = new GetLocationAreaEncounters();
			encounters.setLocationAreaEncounters(response.getBody().getLocationAreaEncounters());
			 
			 return encounters;
		} 
		
		return null;
	}

	@Override
	public GetName getName(String name) {
		ResponseEntity<Pokemon>  response = pokemonClientRest.getPokemon(name.trim().toLowerCase());
		
		if(response.getStatusCode() == HttpStatus.OK) { 
			GetName namePokemon = new GetName();
			namePokemon.setName(response.getBody().getName());
			 
			 return namePokemon;
		}
		
		return null;
	}

}
