package com.bankaya.proxy.test.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.MethodEndpoint;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;

import com.bankaya.proxy.test.endpoint.PokemonEndpoint;
import com.bankaya.proxy.test.service.ITrackingService;

/**
 * 
 * @author Enrique
 *
 */
@Component
public class EndpointInterceptorComponent implements EndpointInterceptor {

	@Autowired
	private ITrackingService trackingService;

	@Override
	public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {

		TransportContext ctx = TransportContextHolder.getTransportContext();
		HttpServletRequest req = ((HttpServletConnection) ctx.getConnection()).getHttpServletRequest();

		if (endpoint instanceof MethodEndpoint) {
			MethodEndpoint methodEndpoint = (MethodEndpoint) endpoint;
			trackingService.save(req, methodEndpoint.getMethod().getName());

			return methodEndpoint.getMethod().getDeclaringClass() == PokemonEndpoint.class;
		}

		return false;
	}

	@Override
	public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
		// TODO Auto-generated method stub

	}
}
