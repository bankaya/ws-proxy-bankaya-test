package com.bankaya.proxy.test.exception;

import lombok.Data;

@Data
public class BusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6548811312709019018L;
	
	private String code;
	private String msg;

	public BusinessException(String code,String msg) {
		super(msg);
		this.code=code;
		this.msg=msg;
    }
}
