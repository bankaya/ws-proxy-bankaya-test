package com.bankaya.proxy.test.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.bankaya.proxy.test.model.ErrorMessage;
import com.bankaya.proxy.test.model.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Enrique
 *
 */
@EnableWebMvc
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalControllerAdvice {

	/**
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage resourceNotFoundException(BusinessException ex, WebRequest request) {
       
        return new ErrorMessage(
                (ex.getCode()!=null?ex.getCode():String.valueOf(HttpStatus.BAD_REQUEST.value())),
                ex.getMessage(),
                null,
                null
           );


    }
    
    /**
     * 
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {

       return new ErrorMessage(
                String.valueOf(HttpStatus.NOT_FOUND.value()),
                ex.getMessage(),
                null,
                null
        );


    }
    
    /**
     * 
     * @param ex
     * @return
     */
    @ExceptionHandler(value = { NoHandlerFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage noHandlerFoundException(Exception ex) {
    	
    	return new ErrorMessage(
                String.valueOf(HttpStatus.NOT_FOUND.value()),
                ex.getMessage(),
                null,
                null
        );
    }
    
    /**
     * 
     * @param ex
     * @param request
     * @return
     */
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorMessage globalExceptionHandler(Exception ex, WebRequest request) {
       
        return new ErrorMessage(
                String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
                ex.getMessage(),
                null,
                null);

    }


    /**
     * 
     * @param ex
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Object>  errorHandler(MethodArgumentNotValidException ex) {

        List<ValidationError> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult()
                .getFieldErrors()) {
            ValidationError build = ValidationError.builder()
                    .errorCode(error.getField())
                    .description(error.getDefaultMessage())
                    .build();
            errors.add(build);
        }
        ErrorMessage  message = new ErrorMessage(String.valueOf(HttpStatus.BAD_REQUEST.value()) ,HttpStatus.BAD_REQUEST.name(), null,errors);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);

    }

}
