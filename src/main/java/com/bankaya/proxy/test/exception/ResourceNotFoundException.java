package com.bankaya.proxy.test.exception;

public class ResourceNotFoundException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2576401850093126695L;

	public ResourceNotFoundException(String msg) {
        super(msg);
    }
}
