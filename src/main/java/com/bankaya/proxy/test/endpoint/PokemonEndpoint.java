package com.bankaya.proxy.test.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.bankaya.proxy.test.schema.GetAbilities;
import com.bankaya.proxy.test.schema.GetBaseExperience;
import com.bankaya.proxy.test.schema.GetHeldItems;
import com.bankaya.proxy.test.schema.GetId;
import com.bankaya.proxy.test.schema.GetLocationAreaEncounters;
import com.bankaya.proxy.test.schema.GetName;
import com.bankaya.proxy.test.service.IPokeProcessorService;

/**
 * 
 * @author Enrique
 *
 */
@Endpoint
public class PokemonEndpoint {

	private static Logger log = LoggerFactory.getLogger(PokemonEndpoint.class);
	
	private static final String NAMESPACE_URI = "http://bankaya.com/guides/ws-proxy-bankaya-test";
	
	@Autowired
	private IPokeProcessorService pokeProcessorService;
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAbilities")
	@ResponsePayload
	public GetAbilities getAbilities(@RequestPayload GetAbilities request) {
		log.info("[getAbilities] - request :: {}", request.getName());
		return pokeProcessorService.getAbilities(request.getName());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBaseExperience")
	@ResponsePayload
	public GetBaseExperience getBaseExperience(@RequestPayload GetBaseExperience request) {
		log.info("[getBaseExperience] - request :: {}", request.getName());
		return pokeProcessorService.getBaseExperience(request.getName());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getHeldItems")
	@ResponsePayload
	public GetHeldItems getHeldItems(@RequestPayload GetHeldItems request) {
		log.info("[getHeldItems] - request :: {}", request.getName());
		return pokeProcessorService.getHeldItems(request.getName());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getId")
	@ResponsePayload
	public GetId getId(@RequestPayload GetId request) {
		log.info("[getId] - request :: {}", request.getName());
		return pokeProcessorService.getId(request.getName());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getName")
	@ResponsePayload
	public GetName getName(@RequestPayload GetName request) {
		log.info("[getName] - request :: {}", request.getName());
		return pokeProcessorService.getName(request.getName());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getLocationAreaEncounters")
	@ResponsePayload
	public GetLocationAreaEncounters getLocationAreaEncounters(@RequestPayload GetLocationAreaEncounters request) {
		log.info("[getLocationAreaEncounters] - request :: {}", request.getName());
		return pokeProcessorService.getLocationAreaEncounters(request.getName());
	}

}
