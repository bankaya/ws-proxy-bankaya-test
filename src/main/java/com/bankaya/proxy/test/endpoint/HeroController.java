package com.bankaya.proxy.test.endpoint;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 * @author Enrique
 *
 */
@RestController
public class HeroController {

	/**
	 * 
	 * @return
	 */
	@GetMapping("/test")
	public ResponseEntity<String> getHeros() {
		return ResponseEntity.ok("test");
	}
	
}
