package com.bankaya.proxy.test.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.bankaya.proxy.test.model.Pokemon;

/**
 * 
 * @author Enrique
 *
 */
@FeignClient(name = "api-pokemon", url = "https://pokeapi.co")
public interface IPokemonClientRest {

	@GetMapping(value = "/api/v2/pokemon/{pokemon}")
	ResponseEntity<Pokemon> getPokemon(@PathVariable(value = "pokemon") String pokemon);

}
