package com.bankaya.proxy.test.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.bankaya.proxy.test.domain.Tracking;

/**
 * 
 * @author Enrique
 *
 */
@Repository
public interface TrackingRepository extends CrudRepository<Tracking, Integer> {

}
