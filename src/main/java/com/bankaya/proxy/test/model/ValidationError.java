package com.bankaya.proxy.test.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValidationError implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("description")
    private String description;
}

