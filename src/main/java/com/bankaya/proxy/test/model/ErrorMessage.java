package com.bankaya.proxy.test.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import java.io.Serializable;
import java.util.List;


@Data
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
    @NonNull
    private String description;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String detail;
    
    @JsonProperty("errors")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ValidationError> errors;
}

