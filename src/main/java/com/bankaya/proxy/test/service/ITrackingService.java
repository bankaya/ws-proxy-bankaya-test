package com.bankaya.proxy.test.service;

import javax.servlet.http.HttpServletRequest;
import com.bankaya.proxy.test.domain.Tracking;

/**
 * 
 * @author Enrique
 *
 */
public interface ITrackingService {

	Tracking save(HttpServletRequest request, String methodName);
}
