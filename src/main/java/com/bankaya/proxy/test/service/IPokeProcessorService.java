package com.bankaya.proxy.test.service;

import com.bankaya.proxy.test.schema.GetAbilities;
import com.bankaya.proxy.test.schema.GetBaseExperience;
import com.bankaya.proxy.test.schema.GetHeldItems;
import com.bankaya.proxy.test.schema.GetId;
import com.bankaya.proxy.test.schema.GetLocationAreaEncounters;
import com.bankaya.proxy.test.schema.GetName;

/**
 * 
 * @author Enrique
 *
 */
public interface IPokeProcessorService {

	GetAbilities getAbilities(String name);
	
	GetBaseExperience getBaseExperience(String name);
	
	GetHeldItems getHeldItems(String name);
	
	GetId getId(String name);
	
	GetLocationAreaEncounters getLocationAreaEncounters(String name);
	
	GetName getName(String name);
}
