package com.bankaya.proxy.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.bankaya.proxy.test.client"})
public class ApiProxyBankayaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProxyBankayaTestApplication.class, args);
	}

}
