//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.2 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2022.01.24 a las 10:49:06 PM CST 
//


package com.bankaya.proxy.test.schema;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bankaya.guides.ws_proxy_bankaya_test package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bankaya.guides.ws_proxy_bankaya_test
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAbilities }
     * 
     */
    public GetAbilities createGetAbilities() {
        return new GetAbilities();
    }

    /**
     * Create an instance of {@link GetBaseExperience }
     * 
     */
    public GetBaseExperience createGetBaseExperience() {
        return new GetBaseExperience();
    }

    /**
     * Create an instance of {@link GetHeldItems }
     * 
     */
    public GetHeldItems createGetHeldItems() {
        return new GetHeldItems();
    }

    /**
     * Create an instance of {@link GetId }
     * 
     */
    public GetId createGetId() {
        return new GetId();
    }

    /**
     * Create an instance of {@link GetName }
     * 
     */
    public GetName createGetName() {
        return new GetName();
    }

    /**
     * Create an instance of {@link GetLocationAreaEncounters }
     * 
     */
    public GetLocationAreaEncounters createGetLocationAreaEncounters() {
        return new GetLocationAreaEncounters();
    }

}
