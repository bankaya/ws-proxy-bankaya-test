
package com.bankaya.proxy.test.schema;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.bankaya.proxy.test.model.Ability;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@Data
@XmlRootElement(name = "getAbilities")
public class GetAbilities {

	@XmlElement(required = true)
	protected String name;

	@XmlElement(name = "abilities")
	private List<Ability> abilities;
}
