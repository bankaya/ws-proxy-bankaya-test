package com.bankaya.proxy.test.utils;


import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Enrique
 *
 */
@Component
public class TrackingUtils {

	public static String getIpFromHttpRequest(HttpServletRequest request) {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}
}
